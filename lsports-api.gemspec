# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "lsports-api/version"

Gem::Specification.new do |spec|
  spec.name          = "lsports-api"
  spec.version       = Lsports::Api::VERSION
  spec.authors       = ["Renato Filho"]
  spec.email         = ["renatosousafilho@gmail.com"]

  spec.summary       = %q{Consume API from lsports}
  spec.description   = %q{Consume API from lsports}
  spec.homepage      = "http://"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "httparty", "~> 0.15.5"
  spec.add_runtime_dependency "recursive-open-struct", "~> 1.0.5"

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
end
