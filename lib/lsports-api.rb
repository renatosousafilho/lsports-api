require 'httparty'
require 'json'
require 'date'
require 'recursive-open-struct'

require "lsports-api/version"
require "lsports-api/client"
require "lsports-api/config"
require "lsports-api/countries_api"
require "lsports-api/sports_api"
require "lsports-api/leagues_api"
require "lsports-api/bet_types_api"
require "lsports-api/bookmakers_api"
require "lsports-api/sport_events_api"

module Lsports
  class << self

    extend Forwardable

    def_delegators :configuration, :username, :password, :guid, :lang
    # def_delegators :configuration, :email, :password, :guid, :lang

    def configuration
      @configuration ||= Lsports::Config.new
    end

    def configure(&block)
      yield configuration
    end
  end
end
