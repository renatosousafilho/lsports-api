module Lsports
  module Api

    class SportEventsAPI
      attr_reader :client

      def initialize
        @client = Client.new
      end

      def base_path
        "GetFixtureMarkets"
      end

      def find_by_leagues(leagues, timestamp=nil)
        client.get(base_path, { leagues: leagues, timestamp: timestamp })
      end

      def find_by_sports(sports, timestamp=nil)
        client.get(base_path, { sports: sports, timestamp:  timestamp })
      end

      def find(params)
        client.get(base_path, params)
      end

    end
  end
end
