module Lsports
  module Api
    class BookmakersAPI
      attr_reader :client

      def initialize
        @client = Client.new
      end

      def base_path
        'GetBookmakers'
      end

      def list
        client.get(base_path)
      end

    end
  end
end
