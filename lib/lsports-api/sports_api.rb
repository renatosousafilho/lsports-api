module Lsports
  module Api
    class SportsAPI
      attr_reader :client

      def initialize
        @client = Client.new
      end

      def base_path
        'GetSports'
      end

      def list
        client.get(base_path)
      end

    end
  end
end
