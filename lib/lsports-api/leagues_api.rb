module Lsports
  module Api
    class LeaguesAPI
      attr_reader :client

      def initialize
        @client = Client.new
      end

      def base_path
        'GetLeagues'
      end

      def list
        client.get(base_path)
      end

    end
  end
end
