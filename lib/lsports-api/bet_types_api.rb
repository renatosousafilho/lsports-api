module Lsports
  module Api
    class BetTypesAPI
      attr_reader :client

      def initialize
        @client = Client.new
      end

      def base_path
        'GetOutcomes'
      end

      def list
        client.get(base_path)
      end

    end
  end
end
