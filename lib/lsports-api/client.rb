module Lsports
  module Api
    class Client
      attr_accessor :params

      def default_params
        {
          username: Lsports.username,
          password: Lsports.password,
          guid: Lsports.guid,
          lang: Lsports.lang
        }
      end

      def headers
        { 'Accept-Encoding': 'gzip, deflate' }
      end

      def base_url
        # "http://json.oddservice.com/OS/OddsWebService.svc"
        "http://prematch.lsports.eu/OddService"
      end

      def get(base_path, params={})
        url ="#{base_url}/#{base_path}?#{url_params(params)}"
        puts url
        response = HTTParty.get(url, headers: headers)
        create_response(response)
      end

      private

      def url_params(params)
        default_params.merge(params).map { |k,v| "#{k}=#{v}"}.join("&")
      end

      def to_snake_case(string)
        string.gsub(/::/, '/').
        gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
        gsub(/([a-z\d])([A-Z])/,'\1_\2').
        tr("-", "_").
        downcase
      end

      def underscore_key(k)
        # Or, if you're not in Rails:
        to_snake_case(k.to_s).to_sym
      end

      def convert_hash_keys(value)
        case value
        when Array
          value.map { |v| convert_hash_keys(v) }
          # or `value.map(&method(:convert_hash_keys))`
        when Hash
          Hash[value.map { |k, v| [underscore_key(k), convert_hash_keys(v)] }]
        else
          value
        end
      end

      def create_response(response)
        json = convert_hash_keys(JSON.parse(response.parsed_response))
        RecursiveOpenStruct.new(json, recurse_over_arrays: true )
      end
    end
  end
end
