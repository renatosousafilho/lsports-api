module Lsports
  module Api
    class CountriesAPI
      attr_reader :client

      def initialize
        @client = Client.new
      end

      def base_path
        'GetCountries'
      end

      def list
        client.get(base_path)
      end

    end
  end
end
